﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using WeatherProject;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Moq;


namespace WeatherProject.Tests
{
    [TestClass()]
    public class DataDispatcherTests
    {
        [TestMethod()]
        public void DataDispatcherTest()
        {
            List<IWeatherDevice> DeviceList = new List<IWeatherDevice>();
            Mock<IWeatherDevice> Device1 = new Mock<IWeatherDevice>();
            Mock<IWeatherDevice> Device2 = new Mock<IWeatherDevice>();
            DeviceList.Add(Device1.Object);
            DeviceList.Add(Device2.Object);
            Mock<IProvider<IWeatherDevice>> weatherProvider = new Mock<IProvider<IWeatherDevice>>();
            weatherProvider.Setup(x => x.getList()).Returns(DeviceList);
            Device1.Setup(x => x.setListener(It.IsAny<IWeatherDeviceListener>()));
            Device2.Setup(x => x.setListener(It.IsAny<IWeatherDeviceListener>()));

            DataDispatcher dataDispatcher = new DataDispatcher(weatherProvider.Object);
            dataDispatcher.Initialize();

            Device1.VerifyAll();
            Device2.VerifyAll();
            weatherProvider.VerifyAll();
                     
        }
    }
}