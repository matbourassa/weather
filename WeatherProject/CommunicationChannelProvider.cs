﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WeatherProject
{
    class CommunicationChannelProvider : IProvider<ICommunicationChannel>
    {
        List<ICommunicationChannel> IProvider<ICommunicationChannel>.getList()
        {
            List<ICommunicationChannel> communicationChannelList = new List<ICommunicationChannel>();
            communicationChannelList.Add(new SMSCommunicationChannel());
            return communicationChannelList;
        }
    }
}
