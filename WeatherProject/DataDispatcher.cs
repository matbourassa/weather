﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WeatherProject
{
    public class DataDispatcher
    {
        private IProvider<IWeatherDevice> weatherProvider;
        private IProvider<ICommunicationChannel> communicationChannelProvider;

        public DataDispatcher(IProvider<IWeatherDevice> weatherProvider, IProvider<ICommunicationChannel> communicationChannel)
        {
            this.weatherProvider = weatherProvider;
            this.communicationChannelProvider = communicationChannel;
        }

        public void Initialize()
        {
            foreach (IWeatherDevice device in weatherProvider.getList())
            {

                device.setListener(new Listener(this));
                device.start();
            }

        }

        public void DispatchData(string data)
        {
            foreach (ICommunicationChannel channel in communicationChannelProvider.getList())
            {

                channel.SendData(data);
            }            
        }


        private class Listener : IWeatherDeviceListener
        {
            private DataDispatcher dataDispatcher;

            public Listener(DataDispatcher dataDispatcher)
            {
                this.dataDispatcher = dataDispatcher;
            }

            public void onDataUpdate(string data)
            {

                dataDispatcher.DispatchData(data);

            }
            
        }

        


    }
}
