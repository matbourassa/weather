﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WeatherProject
{
    public class WeatherDeviceProvider : IProvider<IWeatherDevice>
    {
              

        List<IWeatherDevice> IProvider<IWeatherDevice>.getList()
        {
            
            IWeatherDevice thermometre = new ThermometreWeatherDevice(new ThermoWorksThermometerImpl());
            IWeatherDevice barometer = new BarometerWeatherDevice(new BarometerImpl());

            List<IWeatherDevice> DeviceList = new List<IWeatherDevice>();
            DeviceList.Add(thermometre);
            DeviceList.Add(barometer);

            return DeviceList;
        }



    }
}
