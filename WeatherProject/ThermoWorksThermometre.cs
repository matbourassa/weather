﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

// 
// ThermoWorks Thermometer SDK v2.3
//
// Copyright 2012
//
namespace WeatherProject
{
    public interface ThermoWorksThermometer
    {
        int currentTemperature();
    }

    class ThermoWorksThermometerImpl : ThermoWorksThermometer
    {

        Random random = new Random();

        public int currentTemperature()
        {
            return random.Next(3, 10);
        }

    }
}