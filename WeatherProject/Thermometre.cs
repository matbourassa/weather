﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;

namespace WeatherProject
{
    class ThermometreWeatherDevice : IWeatherDevice
    {

        IWeatherDeviceListener listener;
        ThermoWorksThermometer thermoWorksThermometre;
        Timer thermoTimer = new Timer(1000);


        public ThermometreWeatherDevice(ThermoWorksThermometer thermoWorksThermometre)
        {
            this.thermoWorksThermometre = thermoWorksThermometre;
        }

        public void setListener(IWeatherDeviceListener listener)
        {
            this.listener = listener;

        }

        public void start()
        {

            thermoTimer.Elapsed += OnTimedEvent;
            thermoTimer.AutoReset = true;
            thermoTimer.Enabled = true;

        }

        private void OnTimedEvent(Object source, ElapsedEventArgs e)
        {
            
            int temparature = thermoWorksThermometre.currentTemperature();
            listener.onDataUpdate(temparature.ToString()+ "C");
        }


        public void stop()
        {
            thermoTimer.Stop();
            thermoTimer.Enabled = false;
        }

        
    }
}
