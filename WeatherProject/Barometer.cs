﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WeatherProject
{
    public class BarometerWeatherDevice : IWeatherDevice, PressureCallback
    {
        private Barometer barometer;

        IWeatherDeviceListener listener;

        public BarometerWeatherDevice(Barometer barometer)
        {
            this.barometer = barometer;
        }

        void PressureCallback.onNewPressure(int psi)
        {
            listener.onDataUpdate(psi.ToString() + "kpa");
        }

        void PressureCallback.onPressureError(string error)
        {
            listener.onDataUpdate("an error has occured " +error);
        }

        void IWeatherDevice.setListener(IWeatherDeviceListener listener)
        {
            this.listener = listener;
        }



        void IWeatherDevice.start()
        {

            try
            {
                barometer.setPressureCallback(this);
            }
            catch (Exception e)
            {
                listener.onDataUpdate(e.Message);
            }


        }

        

        void IWeatherDevice.stop()
        {
            barometer.setPressureCallback(null);
           
        }

       

    }
}
