﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WeatherProject
{
    class Program
    {
        static void Main(string[] args)
        {

            
            IProvider<IWeatherDevice> weatherProvider = new WeatherDeviceProvider();
            IProvider<ICommunicationChannel> communicationChannel = new CommunicationChannelProvider();
            DataDispatcher dataDispatcher = new DataDispatcher(weatherProvider, communicationChannel);
            
            dataDispatcher.Initialize();
            Console.ReadLine();


        }




    }
}
