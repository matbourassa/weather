﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;

namespace WeatherProject
{
    public interface Barometer
    {
        void setPressureCallback(PressureCallback callback);
    }

    public interface PressureCallback
    {
        void onNewPressure(int psi);
        void onPressureError(string error);
    }

    public class BarometerNotAvailableException : Exception
    {
        public BarometerNotAvailableException(string message) : base(message)
        {
        }
    }

    class BarometerImpl : Barometer
    {
        PressureCallback callback;
        Random random = new Random();
        System.Timers.Timer aTimer;

        void Barometer.setPressureCallback(PressureCallback callback)
        {
            if (random.Next(1, 3) == 1)
            {
                throw (new BarometerNotAvailableException("Someone probably disconnected the barometer cable."));
            }
            this.callback = callback;

            aTimer = new System.Timers.Timer(10000);
            aTimer.Elapsed += OnTimedEvent;
            aTimer.AutoReset = true;
            aTimer.Enabled = true;
        }

        private void OnTimedEvent(Object source, ElapsedEventArgs e)
        {
            if (random.Next(1, 5) == 1)
            {
                callback.onPressureError("Error 23");
            }
            else
            {
                callback.onNewPressure(random.Next(24, 56));
            }
        }
        
    }
}
